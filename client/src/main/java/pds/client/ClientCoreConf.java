package pds.client;

public class ClientCoreConf {
    private String ipAddress;
    private int listenPort;

    public String getIpAddress() {
        return ipAddress;
    }

    public int getListenPort() {
        return listenPort;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setListenPort(int listenPort) {
        this.listenPort = listenPort;
    }

    @Override
    public String toString() {
        return "ClientCoreConf{" +
                "ipAdress=" + ipAddress +
                ", listenPort=" + listenPort +
                '}';
    }
}
