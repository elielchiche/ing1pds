package pds.client;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Client {
    private final static Logger logger = LoggerFactory.getLogger(Client.class.getName());

    public static void main(String[] args) throws ParseException, IOException {
        final Options opts = new Options();
        final Option clientMode = Option.builder().longOpt("query").hasArg(true).argName("query").build();
        opts.addOption(clientMode);

        final CommandLineParser commandLineParser = new DefaultParser();
        final CommandLine cl = commandLineParser.parse(opts, args);

        if(cl.hasOption("query")) {
            String option = String.valueOf(cl.getOptionValue("query"));
            ClientConf clientConf = new ClientConf();
            ClientRequest clientRequest = new ClientRequest(clientConf, option);
            clientRequest.start();
        }
    }
}
