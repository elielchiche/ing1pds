package pds.client.Frame.RentingOffice.offerslist;


import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import pds.client.Frame.RentingOffice.DataGetter;
import pds.client.Frame.RentingOffice.RentRequest;
import pds.client.Frame.RentingOffice.RentingForm.Rentingform;
import pds.client.Frame.RentingOffice.ValidationMessage;
import pds.client.Frame.RentingOffice.equipmentForm.EquipementForm;
import pds.client.Frame.RentingOffice.offerSelected.equipment;
import pds.client.Frame.RentingOffice.DataSetter;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class OffersList extends JFrame {
    private JPanel topPanel;
    private JTable table;
    private JLabel image;
    private JButton retourButton;
    private JButton reserverButton;
    private JButton reserve;
    private RentRequest rentrequest;
    private DataGetter dataGetter;
    private DataSetter dataSetter;
    private Boolean checkArea;
    ArrayList<HashMap<String, Object>> spaceList;

    private JScrollPane scrollPane;
    private String[] columns = new String[3];
    private String[][] data = new String[3][3];
    JButton button = new JButton();

    public OffersList(RentRequest rentrequest) {
        this.$$$setupUI$$$();
        this.rentrequest = rentrequest;
        ArrayList<Integer> offices = new ArrayList<>();
        setTitle("Liste des offres");
        getContentPane().add(topPanel);
        dataGetter = new DataGetter();
        dataSetter = new DataSetter();

        /*Initialize the table */
        DefaultTableModel model = new DefaultTableModel();
        /*Adding the columns */
        model.addColumn("ID du local");
        model.addColumn("type du local");
        model.addColumn("Superficie");
        model.addColumn("Etage");
        model.addColumn("Prix");
        model.addColumn("Action");
        /* Adding rows from database */
        /* recommendation 1 */
        ArrayList<Double> total = data(model, offices);
        Double total_area = total.get(0);
        Double total_price = total.get(1);
        /* recommendation 2 */
        //data2(model);
        // data3(model);
        if (total_area <= Double.parseDouble(rentrequest.getArea()) && total_price <= rentrequest.getBudget())
            table.setModel(model);
        else
            JOptionPane.showMessageDialog(this, "Pas d'offres disponible");
        table.getColumn("Action").setCellRenderer(new ButtonRenderer());
        table.getColumn("Action").setCellEditor(new ButtonEditor(new JCheckBox()));

        table.setRowHeight(table.getRowHeight() + 20);

        JTableHeader tableHeader = table.getTableHeader();
        tableHeader.setFont(new Font("Calibri", Font.BOLD, 14));
        tableHeader.setBackground(Color.decode("#F0F8FF"));

        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        /* passing the office id to the frame offerSelected where there's all the details about the office and his spaces */
                        int i = table.getSelectedRow();
                        TableModel model = table.getModel();
                        System.out.println("Vous avez choisi la ligne + " + model.getValueAt(i, 1).toString());
                        EquipementForm selected = new EquipementForm(Integer.parseInt(model.getValueAt(i, 0).toString()), rentrequest);
                        selected.setBounds(10, 10, 800, 800);
                        selected.pack();
                        dispose();
                    }
                }
        );

        this.setBounds(300, 90, 2000, 2000);
        this.setContentPane(topPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();

        retourButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /* back button */
                Rentingform rentingform = new Rentingform(rentrequest.getCompany(), rentrequest.getID_company());
                dispose();
                rentingform.setVisible(true);
            }
        });

        reserverButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /* waiting validation from the company */
                int reply = JOptionPane.showConfirmDialog(null, "Etes vous sur ?", "Réservation", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    for (int office_id : offices) {
                        dataSetter.updateRent(office_id, rentrequest.getID_company(), rentrequest.getTotal_price(office_id));
                        for (equipment eq : rentrequest.getEquipments()) {
                            if (eq.getId_office() == office_id)
                                dataSetter.updateEquipment(office_id, eq);
                        }
                    }
                    ValidationMessage message = new ValidationMessage(rentrequest.getCompany());
                    message.setBounds(300, 20, 800, 600);
                    dispose();
                }
            }
        });
    }


    /* fill the table with data from database */
    public ArrayList<Double> data(DefaultTableModel model, ArrayList<Integer> offices) {
        ArrayList<Double> ret = new ArrayList<>();
        Double totalArea = Double.valueOf(0);
        Double totalPrice = Double.valueOf(0);
        try {
            ArrayList<HashMap<String, Object>> space_offer = dataGetter.spaceList(
                    rentrequest.getBureau_ferme(),
                    rentrequest.getSalle_reunion(),
                    Double.parseDouble(rentrequest.getArea()));

            for (HashMap<String, Object> space : space_offer) {
                int ID = (int) space.get("office_id");
                String type = (String) space.get("office_type");
                Double Area_ = (Double) space.get("office_area");
                int floor = (int) space.get("office_floor");
                Double price = (Double) space.get("office_price");
                totalArea += Area_;
                offices.add(ID);
                totalPrice += price;
                if (rentrequest.getTotal_price(ID) != 0)
                    price = rentrequest.getTotal_price(ID);
                else
                    rentrequest.setTotal_price(ID, price);
                model.addRow(new Object[]{ID, type, Area_, floor, price});//Adding row in Table
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ret.add(0, totalArea);
        ret.add(1, totalPrice);
        return ret;
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        topPanel = new JPanel();
        topPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        topPanel.setBackground(new Color(-131098));
        topPanel.setEnabled(false);
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.setBackground(new Color(-5125677));
        topPanel.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, new Dimension(20, 20), null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel2.setBackground(new Color(-1));
        panel1.add(panel2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        image = new JLabel();
        image.setBackground(new Color(-1));
        image.setText("");
        panel2.add(image, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel3.setBackground(new Color(-131098));
        panel2.add(panel3, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$("Calibri", Font.BOLD, 25, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Bureaux disponibles");
        panel3.add(label1, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("  ");
        panel3.add(label2, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setBackground(new Color(-1));
        panel3.add(scrollPane1, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        table = new JTable();
        table.setGridColor(new Color(-6758665));
        scrollPane1.setViewportView(table);
        retourButton = new JButton();
        retourButton.setBackground(new Color(-5776137));
        retourButton.setText("Retour");
        panel3.add(retourButton, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        reserverButton = new JButton();
        reserverButton.setText("Reserver");
        panel3.add(reserverButton, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        Font font = new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
        boolean isMac = System.getProperty("os.name", "").toLowerCase(Locale.ENGLISH).startsWith("mac");
        Font fontWithFallback = isMac ? new Font(font.getFamily(), font.getStyle(), font.getSize()) : new StyleContext().getFont(font.getFamily(), font.getStyle(), font.getSize());
        return fontWithFallback instanceof FontUIResource ? fontWithFallback : new FontUIResource(fontWithFallback);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return topPanel;
    }

    /* add column button to the table */
    class ButtonRenderer extends JButton implements TableCellRenderer {
        public ButtonRenderer() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            setText((value == null) ? "Choisir les équipements" : value.toString());
            return this;
        }
    }

    class ButtonEditor extends DefaultCellEditor {
        private String label;

        public ButtonEditor(JCheckBox checkBox) {
            super(checkBox);
        }

        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            label = (value == null) ? "Séléctionné" : value.toString();
            button.setPreferredSize(new Dimension(10, 10));
            button.setText(label);
            return button;
        }

        public Object getCellEditorValue() {
            return new String(label);
        }


    }
}


