package pds.client.Frame.RentingOffice;

import pds.client.ClientConf;
import pds.client.Frame.RentingOffice.offerSelected.equipment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataSetter {

    private RentRequest rentRequest;
    private HashMap<String, Object> requestMap;

    public void updateRent(int office_id, int company_id, double total_price) {
        try {
            rentRequest = new RentRequest(new ClientConf());
        } catch (IOException e) {
            e.printStackTrace();
        }
        requestMap = new HashMap<>();
        requestMap.put("request", "rentOnceOffice");
        requestMap.put("id_company", company_id);
        requestMap.put("office_id", office_id);
        requestMap.put("rent_price", total_price);
        rentRequest.startUpdate(requestMap);
    }

    public void updateEquipment(int office_id, equipment eq) {
        try {
            rentRequest = new RentRequest(new ClientConf());
        } catch (IOException e) {
            e.printStackTrace();
        }
        requestMap = new HashMap<>();
        requestMap.put("request", "updateequipments");
        requestMap.put("Climatisation", eq.getClimatisation());
        requestMap.put("Chauffage", eq.getChauffage());
        requestMap.put("Ecran", eq.getEcran());
        requestMap.put("Fenetre electro-chromatiques", eq.getFenetre_electro());
        requestMap.put("office_id", office_id);
        rentRequest.startUpdate(requestMap);
    }
}
