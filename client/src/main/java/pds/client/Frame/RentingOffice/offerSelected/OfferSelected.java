package pds.client.Frame.RentingOffice.offerSelected;


import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import pds.client.Frame.RentingOffice.DataGetter;
import pds.client.Frame.RentingOffice.RentRequest;
import pds.client.Frame.RentingOffice.equipmentForm.EquipementForm;
import pds.client.Frame.RentingOffice.offerslist.OffersList;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class OfferSelected extends JFrame {

    private JPanel panel;
    private JLabel titre2;
    private JTable table;
    private JLabel prix;
    private JLabel etage;
    private JLabel superficie;
    private JButton reserve;
    private JButton back;
    private JLabel image;
    private int office_id;
    private RentRequest rentrequest;
    JButton button = new JButton();
    private ArrayList<equipment> equipments;
    private equipment v_equipment;
    private DataGetter dataGetter;

    public equipment getV_equipment() {
        return v_equipment;
    }

    public void setV_equipment(equipment v_equipment) {
        this.v_equipment = v_equipment;
    }

    public ArrayList<equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(equipment equipment) {
        equipments = new ArrayList<equipment>(30);
        equipments.add(equipment);
    }

    public int getOffice_id() {
        return office_id;
    }

    public void setOffice_id(int office_id) {
        this.office_id = office_id;
    }


    public OfferSelected(int office_id, RentRequest rentrequest) {
        this.$$$setupUI$$$();
        double total;
        equipments = rentrequest.getEquipments();
        this.rentrequest = rentrequest;
        this.office_id = office_id;
        getContentPane().add(panel);
        v_equipment = new equipment();
        v_equipment = rentrequest.getV_equipment();

        dataGetter = new DataGetter();
        HashMap<String, Object> office = null;
        try {
            office = dataGetter.officeData(office_id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        titre2.setText((String) office.get("office_type"));
        etage.setText("Etage numéro : " + office.get("office_floor"));
        if (equipmentPrice() > 0) {
            total = (double) office.get("office_price") + equipmentPrice();
            prix.setText("Prix : " + total + " EURO (prix des équipements inclus)");
        } else {
            total = (double) office.get("office_price");
            prix.setText("Prix : " + total + " EURO ");
        }
        rentrequest.setTotal_price(office_id, total);
        superficie.setText("Superficie : " + office.get("office_area") + " mètres carrés");

        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Equipement");
        model.addColumn("Quantité");
        data(model, v_equipment);
        JTableHeader tableHeader = table.getTableHeader();
        tableHeader.setFont(new Font("Calibri", Font.BOLD, 14));
        tableHeader.setBackground(Color.decode("#F0F8FF"));

        table.setModel(model);
        table.setRowHeight(table.getRowHeight() + 20);
        //table.getColumn("Action").setCellRenderer(new ButtonRenderer());
        //table.getColumn("Action").setCellEditor(new ButtonEditor(new JCheckBox()));

        button.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        /* if the company want to update equipment for a space that has already added so we will retreive data that she has already added to update it*/
                        if (!equipments.isEmpty()) {
                            EquipementForm selected = new EquipementForm(office_id, rentrequest);
                            selected.setBounds(300, 20, 800, 600);
                        }
                    }
                }
        );

        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OffersList of = new OffersList(rentrequest);
                dispose();
                of.setVisible(true);
            }
        });

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
        this.pack();

        reserve.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /* waiting validation from the company */
                OffersList offersList = new OffersList(rentrequest);
                offersList.setBounds(10, 10, 1000, 500);
                offersList.setVisible(true);
                dispose();
            }
        });
    }

    public void data(DefaultTableModel model, equipment v_equipment) {
        if (v_equipment.getChauffage() > 0)
            model.addRow(new Object[]{"Chauffage", v_equipment.getChauffage()});
        if (v_equipment.getEcran() > 0)
            model.addRow(new Object[]{"Ecran", v_equipment.getEcran()});
        if (v_equipment.getClimatisation() > 0)
            model.addRow(new Object[]{"Climatisateur", v_equipment.getClimatisation()});
        if (v_equipment.getFenetre_electro() > 0)
            model.addRow(new Object[]{"Fenetre electro-chromatiques", v_equipment.getFenetre_electro()});
    }

    public void Closeframes() {
        Frame[] allFrames = JFrame.getFrames();

        for (Frame fr : allFrames) {
            fr.dispose();
        }
    }

    /* Setting price of equipments added to each space on the office */
    /* int because  : 20 EURO for each equipment*/
    public int equipmentPrice() {
        int prix = 0;
        for (equipment e : equipments) {
            prix += e.getClimatisation() + e.getChauffage() + e.getFenetre_electro() + e.getEcran();
        }
        System.out.println("le prix est " + prix);

        prix = prix * 20;
        return prix;
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel = new JPanel();
        panel.setLayout(new GridLayoutManager(12, 5, new Insets(0, 0, 0, 0), -1, -1));
        panel.setBackground(new Color(-1));
        final JLabel label1 = new JLabel();
        label1.setText(" ");
        panel.add(label1, new GridConstraints(0, 1, 1, 4, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel.add(spacer1, new GridConstraints(9, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        titre2 = new JLabel();
        Font titre2Font = this.$$$getFont$$$("Calibri", Font.BOLD, 24, titre2.getFont());
        if (titre2Font != null) titre2.setFont(titre2Font);
        titre2.setText("Label");
        panel.add(titre2, new GridConstraints(1, 1, 1, 4, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText(" ");
        panel.add(label2, new GridConstraints(2, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        prix = new JLabel();
        prix.setText("Prix");
        panel.add(prix, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText(" ");
        panel.add(label3, new GridConstraints(6, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        etage = new JLabel();
        etage.setText("Etage");
        panel.add(etage, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        superficie = new JLabel();
        superficie.setText("Supérficie");
        panel.add(superficie, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Les équipements choisis :");
        panel.add(label4, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setBackground(new Color(-1));
        panel.add(scrollPane1, new GridConstraints(8, 1, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        table = new JTable();
        scrollPane1.setViewportView(table);
        reserve = new JButton();
        reserve.setText("Valider");
        panel.add(reserve, new GridConstraints(10, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        back = new JButton();
        back.setText("Retour");
        panel.add(back, new GridConstraints(11, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel.add(panel1, new GridConstraints(0, 0, 12, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel2.setBackground(new Color(-1));
        panel1.add(panel2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        image = new JLabel();
        image.setBackground(new Color(-1));
        image.setText("");
        panel2.add(image, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        Font font = new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
        boolean isMac = System.getProperty("os.name", "").toLowerCase(Locale.ENGLISH).startsWith("mac");
        Font fontWithFallback = isMac ? new Font(font.getFamily(), font.getStyle(), font.getSize()) : new StyleContext().getFont(font.getFamily(), font.getStyle(), font.getSize());
        return fontWithFallback instanceof FontUIResource ? fontWithFallback : new FontUIResource(fontWithFallback);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel;
    }


    class ButtonRenderer extends JButton implements TableCellRenderer {
        public ButtonRenderer() {
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            setText((value == null) ? "réserver des équipements" : value.toString());
            return this;
        }
    }

    class ButtonEditor extends DefaultCellEditor {
        private String label;

        public ButtonEditor(JCheckBox checkBox) {
            super(checkBox);
        }

        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected, int row, int column) {
            label = (value == null) ? "Supprimer" : value.toString();
            button.setPreferredSize(new Dimension(10, 10));
            button.setText(label);
            return button;
        }

        public Object getCellEditorValue() {
            return new String(label);
        }


    }


}
