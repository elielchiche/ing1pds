package pds.client.Frame.RentingOffice;

import pds.client.ClientConf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataGetter {

    private RentRequest rentRequest;
    private ArrayList<HashMap<String, Object>> answer;
    private HashMap<String, Object> request;


    public ArrayList<HashMap<String, Object>> companyData() throws IOException {
        rentRequest = new RentRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "companylist");
        answer = rentRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> companyDataId(String name) throws IOException {
        rentRequest = new RentRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "companyId");
        request.put("name", name);
        answer = rentRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> spaceList(int bureau, int salle, double area) throws IOException {
        rentRequest = new RentRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "possibleSpace");
        request.put("bureau",bureau);
        request.put("salle",salle);
        request.put("area",area);
        answer = rentRequest.start(request);
        return(answer);
    }

    public HashMap<String, Object> officeData(int office_id) throws IOException {
        rentRequest = new RentRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "officedata");
        request.put("office_id", office_id);
        answer = rentRequest.start(request);
        return(answer.get(0));
    }
}
