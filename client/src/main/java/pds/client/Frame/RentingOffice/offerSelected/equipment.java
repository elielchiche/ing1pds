package pds.client.Frame.RentingOffice.offerSelected;

public class equipment {

    private int id_office;
    private int Fenetre_electro;
    private int Chauffage;
    private int Ecran;
    private int Climatisation;

    public equipment(int id_office, int fenetre_electro, int chauffage, int ecran, int climatisation) {
        this.id_office = id_office;
        Fenetre_electro = fenetre_electro;
        Chauffage = chauffage;
        Ecran = ecran;
        Climatisation = climatisation;
    }

    public int getId_office() {
        return id_office;
    }

    public void setId_space(int id_office) {
        this.id_office = id_office;
    }

    public int getFenetre_electro() {
        return Fenetre_electro;
    }

    public void setFenetre_electro(int fenetre_electro) {
        Fenetre_electro = fenetre_electro;
    }

    public int getChauffage() {
        return Chauffage;
    }

    public void setChauffage(int chauffage) {
        Chauffage = chauffage;
    }

    public int getEcran() {
        return Ecran;
    }

    public void setEcran(int ecran) {
        Ecran = ecran;
    }

    public int getClimatisation() {
        return Climatisation;
    }

    public void setClimatisation(int climatisation) {
        Climatisation = climatisation;
    }
    public equipment(){
        this.id_office = 0;
        Fenetre_electro = 0;
        Chauffage = 0;
        Ecran = 0;
        Climatisation = 0;
    }
}
