package pds.client.Frame.RentingOffice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pds.client.ClientConf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class AnalyseRequest implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(AnalyseRequest.class.getName());
    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;

    public AnalyseRequest(final ClientConf clientConf) {
        try {
            socket = new Socket(clientConf.getConfig().getIpAddress(), clientConf.getConfig().getListenPort());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<HashMap<String, Object>> start(HashMap<String, Object> map) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        byte[] inputData;
        try {
            inputStream = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write(mapper.writeValueAsBytes(map));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Request submitted");
        while(inputStream.available() == 0) {}
        inputData = new byte[inputStream.available()];
        inputStream.read(inputData);
        ArrayList<HashMap<String, Object>> arrayAnswer = mapper.readValue(inputData, ArrayList.class);
        System.out.println("map : " + arrayAnswer);
        return(arrayAnswer);
    }

}
