package pds.client.Frame.RentingOffice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidNullException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pds.client.ClientConf;
import pds.client.Frame.RentingOffice.offerSelected.equipment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class RentRequest {
    private static final Logger logger = LoggerFactory.getLogger(RentRequest.class.getName());
    private String budget;
    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private int ID_company;
    private String company;
    private String type_local;
    private String Area;
    private int Bureau_ferme;
    private int Espace_indivicuel;
    private int Salle_reunion;
    private HashMap<Integer, Double> officesPrice;
    private equipment v_equipment;


    public double getTotal_price(int office_id) {
        if (officesPrice.get(office_id) != null)
            return officesPrice.get(office_id);
        else
            return 0;
    }

    public void setTotal_price(int office_id, double total_price) {
        this.officesPrice.put(office_id, total_price);
    }

    private ArrayList<equipment> equipments=  new ArrayList<equipment>(30);

    public RentRequest(final ClientConf clientConf) {
        try {
            socket = new Socket(clientConf.getConfig().getIpAddress(), clientConf.getConfig().getListenPort());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<HashMap<String, Object>> start(HashMap<String, Object> map) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        byte[] inputData;
        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            outputStream.write(mapper.writeValueAsBytes(map));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Request submitted");
        while(inputStream.available() == 0) {}
        inputData = new byte[inputStream.available()];
        inputStream.read(inputData);
        ArrayList<HashMap<String, Object>> arrayAnswer = mapper.readValue(inputData, ArrayList.class);
        return(arrayAnswer);
    }

    public void startUpdate(HashMap<String, Object> map) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            outputStream = socket.getOutputStream();
            outputStream.write(mapper.writeValueAsBytes(map));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Request submitted");
    }

    public ArrayList<equipment> getEquipments() {

        return equipments;
    }

    public void setEquipments(equipment eq) {
        equipments.add(eq);
        for(equipment e : equipments){
            System.out.println("en liste "+e.getId_office());
        }
    }

    public void UpdateEquipments(equipment eq) {
        int index =0;
        for(equipment e : equipments){
            if(e.getId_office() == eq.getId_office()){
                index = equipments.indexOf(e);
                break;
            }
        }
        equipments.set(index, eq);
    }

    public equipment getV_equipment() {
        return v_equipment;
    }

    public void setV_equipment(equipment v_equipment) {
        this.v_equipment = v_equipment;
    }

    public int getSalle_reunion() {
        return Salle_reunion;
    }

    public void setSalle_reunion(int salle_reunion) {
        Salle_reunion = salle_reunion;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getType_local() {
        return type_local;
    }

    public void setType_local(String type_local) {
        this.type_local = type_local;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public int getBureau_ferme() {
        return Bureau_ferme;
    }

    public void setBureau_ferme(int bureau_ferme) {
        Bureau_ferme = bureau_ferme;
    }

    public int getEspace_indivicuel() {
        return Espace_indivicuel;
    }

    public void setEspace_indivicuel(int espace_indivicuel) {
        Espace_indivicuel = espace_indivicuel;
    }

    public double getBudget() {
        return Double.parseDouble(budget);
    }

    public RentRequest(int ID_company , String company , String type_local, String area,
                       int bureau_ferme, int salle_reunion, String budget) {
        this.ID_company = ID_company;
        this.company = company;
        this.type_local = type_local;
        this.budget = budget;
        Area = area;
        Bureau_ferme = bureau_ferme;
        Salle_reunion = salle_reunion;
        v_equipment = new equipment();
        equipments = new ArrayList<equipment>(30);
        officesPrice = new HashMap<>();
    }

    public int getID_company() {
        return ID_company;
    }

    public void setID_company(int ID_company) {
        this.ID_company = ID_company;
    }

    public RentRequest(){

    }
}