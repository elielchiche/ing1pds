package pds.client.Frame.RentingOffice.equipmentForm;


import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import pds.client.Frame.RentingOffice.RentRequest;
import pds.client.Frame.RentingOffice.offerSelected.OfferSelected;
import pds.client.Frame.RentingOffice.offerSelected.equipment;
import pds.client.Frame.RentingOffice.offerslist.OffersList;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class EquipementForm extends JFrame {
    private JButton suivantButton;
    private JButton Back;

    private JSpinner fenetre_electro;
    private int Fenetre_electro;

    private JSpinner chauffage;
    private int Chauffage;

    private JSpinner ecran;
    private int Ecran;

    private JSpinner climatisation;
    private JPanel panel;
    private JLabel titre;
    private int Climatisation;

    private int office_id;
    private ArrayList<JSpinner> ArrList;

    private RentRequest rentrequest;
    private equipment c_equipment;

    /* the constructor when it s the first time to setting the equipment for a space */
    public EquipementForm(int office_id,
                          RentRequest rentrequest) {

        super();
        this.$$$setupUI$$$();
        this.office_id = office_id;
        this.rentrequest = rentrequest;
        titre.setText("l'espace numéro " + office_id);

        setTitle("les équipements");
        setJSpinnerValues();
        getContentPane().add(panel);
        getContentPane().setSize(800, 800);

        suivantButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Fenetre_electro = (int) fenetre_electro.getValue();
                Chauffage = (int) chauffage.getValue();
                Ecran = (int) ecran.getValue();
                Climatisation = (int) climatisation.getValue();

                c_equipment = new equipment(office_id, Fenetre_electro,
                        Chauffage, Ecran, Climatisation);

                /* adding c_equipment in the table of equipment in RENTrequest class*/
                rentrequest.setV_equipment(c_equipment);
                rentrequest.setEquipments(c_equipment);
                Closeframes();
                OfferSelected offre = new OfferSelected(office_id, rentrequest);
                offre.setBounds(10, 10, 800, 600);
                dispose();
            }
        });

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(true);


        Back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OffersList offersList = new OffersList(rentrequest);
                offersList.setBounds(10, 10, 1500, 800);
                offersList.setVisible(true);
                dispose();
            }
        });
    }

    /* the constructor to use when the company want to update equipment already aded */
    public EquipementForm(int office_id,
                          RentRequest rentrequest, equipment e) {
        this.office_id = office_id;
        this.rentrequest = rentrequest;
        this.c_equipment = e;
        this.office_id = e.getId_office();
        titre.setText("l'espace numéro " + this.office_id);


        setTitle("les équipements");
        setJSpinnerValues();
        setOldData();
        getContentPane().add(panel);

        suivantButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Fenetre_electro = (int) fenetre_electro.getValue();
                Chauffage = (int) chauffage.getValue();
                Ecran = (int) ecran.getValue();
                Climatisation = (int) climatisation.getValue();

                c_equipment = new equipment(EquipementForm.this.office_id, Fenetre_electro,
                        Chauffage, Ecran, Climatisation);

                rentrequest.setV_equipment(c_equipment);
                rentrequest.UpdateEquipments(c_equipment);

                Closeframes();
                OfferSelected offre = new OfferSelected(office_id, rentrequest);
                //offre.setBounds(10, 10, 800, 600);
                offre.pack();
                dispose();
            }
        });
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
        this.pack();
    }

    public void setOldData() {
        fenetre_electro.setValue(c_equipment.getFenetre_electro());
        chauffage.setValue(c_equipment.getChauffage());
        ecran.setValue(c_equipment.getEcran());
        climatisation.setValue(c_equipment.getClimatisation());
    }

    /*Set the spinner's data */
    public void setJSpinnerValues() {
        int min = 0;
        int max = 10;
        int step = 1;
        int i = 0;
        ArrList = new ArrayList<JSpinner>(
                Arrays.<JSpinner>asList(
                        fenetre_electro
                        , chauffage
                        , ecran
                        , climatisation));

        for (JSpinner element : ArrList) {
            /* Set the the valid range for the spinner between 0 to 10) */
            SpinnerModel value = new SpinnerNumberModel(i, min, max, step);
            element.setModel(value);
        }
    }

    public void Closeframes() {
        Frame[] allFrames = JFrame.getFrames();

        for (Frame fr : allFrames) {
            String specificFrameName = fr.getClass().getName();
            fr.dispose();
        }
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel = new JPanel();
        panel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(10, 4, new Insets(0, 0, 0, 0), -1, -1));
        panel1.setBackground(new Color(-1));
        panel.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$("Calibri", Font.BOLD, 25, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setText("Réserver des équipements");
        panel1.add(label1, new GridConstraints(4, 0, 1, 4, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(9, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(10, 3, new Insets(0, 0, 0, 0), -1, -1));
        panel2.setBackground(new Color(-1));
        Font panel2Font = this.$$$getFont$$$(null, -1, 12, panel2.getFont());
        if (panel2Font != null) panel2.setFont(panel2Font);
        panel1.add(panel2, new GridConstraints(9, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel2.add(spacer2, new GridConstraints(8, 0, 2, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        suivantButton = new JButton();
        suivantButton.setBackground(new Color(-6758665));
        Font suivantButtonFont = this.$$$getFont$$$(null, Font.BOLD, -1, suivantButton.getFont());
        if (suivantButtonFont != null) suivantButton.setFont(suivantButtonFont);
        suivantButton.setText("Confirmer");
        panel2.add(suivantButton, new GridConstraints(8, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        titre = new JLabel();
        titre.setText("          ");
        panel2.add(titre, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("            ");
        panel2.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setForeground(new Color(-16777216));
        label3.setText("Nombre d'équipements");
        panel2.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$(null, Font.ITALIC, -1, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setForeground(new Color(-11974585));
        label4.setText("Fenêtre electro-chromatiques ");
        panel2.add(label4, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$(null, Font.ITALIC, -1, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setForeground(new Color(-11974585));
        label5.setText("Chauffage");
        panel2.add(label5, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        Font label6Font = this.$$$getFont$$$(null, Font.ITALIC, -1, label6.getFont());
        if (label6Font != null) label6.setFont(label6Font);
        label6.setForeground(new Color(-11974585));
        label6.setText("Écran");
        panel2.add(label6, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        Font label7Font = this.$$$getFont$$$(null, Font.ITALIC, -1, label7.getFont());
        if (label7Font != null) label7.setFont(label7Font);
        label7.setForeground(new Color(-11974585));
        label7.setText("Climatisation");
        panel2.add(label7, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        fenetre_electro = new JSpinner();
        panel2.add(fenetre_electro, new GridConstraints(4, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        chauffage = new JSpinner();
        panel2.add(chauffage, new GridConstraints(5, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        ecran = new JSpinner();
        panel2.add(ecran, new GridConstraints(6, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        climatisation = new JSpinner();
        panel2.add(climatisation, new GridConstraints(7, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Back = new JButton();
        Back.setBackground(new Color(-5776137));
        Font BackFont = this.$$$getFont$$$(null, Font.BOLD, -1, Back.getFont());
        if (BackFont != null) Back.setFont(BackFont);
        Back.setText("Annuler");
        panel2.add(Back, new GridConstraints(9, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        label8.setText("+ 20 Euro pour chaque équipement");
        panel2.add(label8, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label9 = new JLabel();
        label9.setText("             ");
        panel1.add(label9, new GridConstraints(1, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel3.setBackground(new Color(-1));
        panel1.add(panel3, new GridConstraints(0, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label10 = new JLabel();
        label10.setText("            ");
        panel1.add(label10, new GridConstraints(8, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label11 = new JLabel();
        label11.setText("        ");
        panel1.add(label11, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label12 = new JLabel();
        label12.setText("          ");
        panel1.add(label12, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label13 = new JLabel();
        label13.setText("          ");
        panel1.add(label13, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label14 = new JLabel();
        label14.setText("              ");
        panel1.add(label14, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label15 = new JLabel();
        label15.setText("             ");
        panel1.add(label15, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        Font font = new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
        boolean isMac = System.getProperty("os.name", "").toLowerCase(Locale.ENGLISH).startsWith("mac");
        Font fontWithFallback = isMac ? new Font(font.getFamily(), font.getStyle(), font.getSize()) : new StyleContext().getFont(font.getFamily(), font.getStyle(), font.getSize());
        return fontWithFallback instanceof FontUIResource ? fontWithFallback : new FontUIResource(fontWithFallback);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel;
    }

}

