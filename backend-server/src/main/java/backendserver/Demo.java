package backendserver;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Demo {

    public static ServerConfig config;
    private static final Logger logger = LoggerFactory.getLogger(ClientRequestManager.class.getName());

    public static void main(String[] args) throws IOException, ParseException {

        final Options opts = new Options();
        final Option saturationMode = Option.builder().longOpt("saturationDelay").hasArg(true).argName("saturationDelay").build();
        final Option serverMode = Option.builder().longOpt("serverMode").build();
        opts.addOption(saturationMode);
        opts.addOption(serverMode);

        final CommandLineParser commandLineParser = new DefaultParser();
        final CommandLine cl = commandLineParser.parse(opts, args);

        if(cl.hasOption("saturationDelay")) {
            int delay_value = 0;
            try {
                delay_value = Integer.parseInt(cl.getOptionValue("saturationDelay"));
            }catch (NullPointerException e) {
                logger.info("no delay defined !");
            }
            logger.info("Server started in saturation Mode!");
            config = new ServerConfig();
            Server server = new Server(config);
            server.serve(delay_value);
        }

        if(cl.hasOption("serverMode")) {
            logger.info("Server started!");
            config = new ServerConfig();
            Server server = new Server(config);
            server.serve(0);
        }
    }
}