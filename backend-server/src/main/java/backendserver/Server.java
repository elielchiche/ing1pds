package backendserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class Server {
    private ServerSocket serverSocket;
    private final static Logger logger = LoggerFactory.getLogger(Server.class.getName());

    public Server(final ServerConfig config) throws IOException {
        serverSocket = new ServerSocket(config.getConfig().getListenPort());
    }

    public void serve(int delay) throws IOException {
        try {
            while (true) {
                logger.info("waiting for client..");
                Socket socket = serverSocket.accept();
                logger.info("Client IP : " + socket.getInetAddress().toString());
                ClientRequestManager manager = new ClientRequestManager(socket, delay);
                try {
                    manager.join();
                    manager.start();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (SocketTimeoutException e) {
            logger.info("Got a timeout ");
        }
    }

}