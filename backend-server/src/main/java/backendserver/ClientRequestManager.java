package backendserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import connectionpool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class ClientRequestManager implements Runnable{
    private final static DataSource ds = DataSource.getInstance();
    private static final Logger logger = LoggerFactory.getLogger(ClientRequestManager.class.getName());
    private final InputStream in;
    private final OutputStream out;
    private final static String name = "client-thread-manager";
    private Thread self;
    private Connection c;
    private final int delay;

    public ClientRequestManager(Socket socket, int delay) throws IOException {
        in = socket.getInputStream();
        out = socket.getOutputStream();
        self = new Thread(this, name);
        this.delay = delay;
    }

    public void join() throws InterruptedException {
        self.join();
    }

    @Override
    public void run() {
        byte inputData[];
        try{
            while (in.available() == 0){}
            inputData = new byte[in.available()];
            in.read(inputData);
            logger.info("data received {}", new String(inputData));
            final ObjectMapper mapper = new ObjectMapper();
            DataOutputStream dos = new DataOutputStream(out);
            c = ds.getConnection();
            requestManager(mapper, dos, c, inputData);
            ds.putBackConnection(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestManager(ObjectMapper mapper, DataOutputStream dos, Connection c, byte[] inputData) throws Exception {
        HashMap<String, Object> requestMap = mapper.readValue(inputData, HashMap.class);
        logger.info("request : " + requestMap.get("request"));
        if (requestMap.get("request").equals("read")){
            byte[] response = Crud.select();
            dos.write(response);
            logger.info("read request data sent!");
        }

        if(requestMap.get("request").equals("insert")){
            String response = Crud.insert((String)requestMap.get("last_name"), (String)requestMap.get("first_name"), (int)requestMap.get("age"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("insert request done");
        }

        if(requestMap.get("request").equals("delete")){
            String response = Crud.delete((String)requestMap.get("last_name"), (String)requestMap.get("first_name"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("delete request done");
        }

        if(requestMap.get("request").equals("update")){
            String response = Crud.update((String)requestMap.get("first_name"), (String)requestMap.get("last_name"), (String) requestMap.get("new_first_name"), (String) requestMap.get("new_last_name"), (int) requestMap.get("new_age"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("update request done");
        }

        if (requestMap.get("request").equals("companylist")) {
            Connection connection = ds.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT name FROM company");
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while (rs.next()) {
                HashMap<String, Object> row = new HashMap<>();
                row.put("name", rs.getString("name"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if (requestMap.get("request").equals("companyId")) {
            Connection connection = ds.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT id_company FROM company WHERE name='"+requestMap.get("name")+"'");
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while (rs.next()) {
                HashMap<String, Object> row = new HashMap<>();
                row.put("id_company", rs.getString("id_company"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if (requestMap.get("request").equals("officedata")) {
            Connection connection = ds.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM office WHERE office_id = ?");
            statement.setInt(1, (int) requestMap.get("office_id"));
            ResultSet rs = statement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while (rs.next()) {
                HashMap<String, Object> row = new HashMap<>();
                row.put("office_id", rs.getInt("office_id"));
                row.put("office_type", rs.getString("office_type"));
                row.put("office_floor", rs.getInt("office_floor"));
                row.put("office_area", rs.getDouble("office_area"));
                row.put("office_price", rs.getDouble("office_price"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if (requestMap.get("request").equals("possibleSpace")) {
            Connection connection = ds.getConnection();
            int nbBureau = (int) requestMap.get("bureau");
            int nbSalleReunion = (int) requestMap.get("salle");
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "(SELECT * FROM office where office.rented = false and office_type='Bureau fermé' ORDER BY office_area ASC LIMIT ?) UNION (SELECT * FROM office where office.rented = false and office_type= 'Salle de réunion' ORDER BY office_area ASC LIMIT ?)");
            preparedStatement.setInt(1, nbBureau);
            preparedStatement.setInt(2, nbSalleReunion);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while (rs.next()) {
                HashMap<String, Object> row = new HashMap<>();
                row.put("office_id", rs.getInt("office_id"));
                row.put("office_type", rs.getString("office_type"));
                row.put("office_floor", rs.getInt("office_floor"));
                row.put("office_area", rs.getDouble("office_area"));
                row.put("office_price", rs.getDouble("office_price"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if (requestMap.get("request").equals("rentOnceOffice")) {
            Connection connection = ds.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE office SET rented = TRUE WHERE office_id=?");
            preparedStatement.setInt(1, (int) requestMap.get("office_id"));
            int rowsAffected = preparedStatement.executeUpdate();
            PreparedStatement preparedStatement2 = connection.prepareStatement("INSERT INTO rent (office_id ,id_company, rent_price) VALUES (?,?,?)");
            preparedStatement2.setInt(1, (int) requestMap.get("office_id"));
            preparedStatement2.setInt(2, (int) requestMap.get("id_company"));
            preparedStatement2.setDouble(3, (double) requestMap.get("rent_price"));
            rowsAffected = preparedStatement2.executeUpdate();
        }

        if (requestMap.get("request").equals("updateequipments")){
            int office_id = (int) requestMap.get("office_id");
            int fenetre = (int) requestMap.get("Fenetre electro-chromatiques");
            int ecran = (int) requestMap.get("Ecran");
            int chauffage = (int) requestMap.get("Chauffage");
            int clim = (int) requestMap.get("Climatisation");
            if (fenetre > 0) {
                PreparedStatement ps = c.prepareStatement("INSERT INTO spaceequipments (equipement, quantity, id_space) VALUES (?, ?, ?)");
                ps.setInt(2, fenetre);
                ps.setString(1, "Fenetre electro-chromatiques");
                ps.setInt(3, office_id);
                int rs3 = ps.executeUpdate();
            }

            if (chauffage>0) {
                PreparedStatement ps = c.prepareStatement("INSERT INTO spaceequipments (equipement, quantity, id_space) VALUES (?, ?, ?)");
                ps.setInt(2, chauffage);
                ps.setString(1, "Chauffage");
                ps.setInt(3, office_id);
                int rs3 = ps.executeUpdate();
            }

            if (ecran>0) {
                PreparedStatement ps = c.prepareStatement("INSERT INTO spaceequipments (equipement, quantity, id_space) VALUES (?, ?, ?)");
                ps.setInt(2, ecran);
                ps.setString(1, "Ecran");
                ps.setInt(3, office_id);
                int rs3 = ps.executeUpdate();
            }

            if (clim>0) {
                PreparedStatement ps = c.prepareStatement("INSERT INTO spaceequipments (equipement, quantity, id_space) VALUES (?, ?, ?)");
                ps.setInt(2, clim);
                ps.setString(1, "Climatisation");
                ps.setInt(3, office_id);
                int rs3 = ps.executeUpdate();
                logger.info("{} rows updated", rs3);
            }
        }
    }

    public void start() {
        self.start();
    }
}
