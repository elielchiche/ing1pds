package backendserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import connectionpool.DataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Crud {
    private final static DataSource ds = DataSource.getInstance();
    public static String delete(String last_name, String first_name) throws SQLException {
        int n = 0;
        Connection c = ds.getConnection();
        Statement statement = c.createStatement();
        n += statement.executeUpdate("DELETE FROM personnes WHERE last_name = '" + last_name +"' and first_name = '" + first_name + "'");
        ds.putBackConnection(c);
        return n + " lines deleted";
    }

    public static byte[] select() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
        Connection c = ds.getConnection();
        Statement stmt = c.createStatement();
        StringBuilder result =  new StringBuilder();
        ResultSet rs = stmt.executeQuery("SELECT * FROM personnes");
        while (rs.next()) {
            HashMap<String, Object> row = new HashMap<>();
            row.put("id_user", rs.getInt("id_user"));
            row.put("first_name", rs.getString("first_name"));
            row.put("last_name", rs.getString("last_name"));
            row.put("age", rs.getInt("age"));
            rowList.add(row);
        }
        ds.putBackConnection(c);
        return mapper.writeValueAsBytes(rowList);
    }

    public static String insert(String last_name,String first_name, int age) throws Exception {
        Connection c = ds.getConnection();
        int n;
        Statement stmt = c.createStatement();
        n = stmt.executeUpdate("INSERT INTO personnes(first_name, last_name, age) VALUES ('" + first_name + "', '" + last_name + "', '" + age +"')");
        ds.putBackConnection(c);
        return n + " lines added";
    }

    public static String update(String first_name, String last_name, String new_first_name, String new_last_name, int new_age) throws Exception {
        Connection c = ds.getConnection();
        int n;
        PreparedStatement pstmt = c.prepareStatement(
                "UPDATE personnes SET first_name = ?, last_name = ?, age = ? " +
                        "WHERE first_name = ? AND last_name = ?"
        );
        pstmt.setString(1, new_first_name);
        pstmt.setString(2, new_last_name);
        pstmt.setInt(3, new_age);
        pstmt.setString(4, first_name);
        pstmt.setString(5, last_name);
        n = pstmt.executeUpdate();
        ds.putBackConnection(c);
        return n + "lines updated";
    }
}
