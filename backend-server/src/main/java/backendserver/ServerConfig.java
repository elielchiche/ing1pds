package backendserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ServerConfig {
    private static final Logger logger = LoggerFactory.getLogger(ServerConfig.class.getName());
    private static final String serverConfigEnvVar = "EPISEN_SRV_CONFIG";
    private final String serverConfigFileLocation;
    private ServerCoreConfig config;

    public ServerConfig() throws IOException {
        serverConfigFileLocation = System.getenv(serverConfigEnvVar);
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        config = mapper.readValue(new File(serverConfigFileLocation), ServerCoreConfig.class);
    }

    public ServerCoreConfig getConfig() {
        return config;
    }
}
